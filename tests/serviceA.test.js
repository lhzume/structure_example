const sinon = require('sinon');
const assert = require('assert');

describe('serviceA', () => {
  describe('doSomething()', () => {

    const resourceMock = {
      updateSomething: sinon.stub()
    };

    resourceMock.updateSomething.returns(42);

    const service = new (require('../serviceA'))({
      resource: resourceMock
    });

    it('should do something', () => {
      assert.ok(service.doSomething())
    });
  });
});
