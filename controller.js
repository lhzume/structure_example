module.exports = class Controller {
  constructor(services) {
    this.services = services;
  }

  doSomething((req, res, next) => {
    this.services['serviceA'].doSomething(req.query['something'])
    .then(result => res.ok(result.data))
    .catch(next);
  });

  doSomethingOther((req, res, next) => {
    this.services['serviceB'].doSomething(req.query['something'])
    .then(res.ok)
    .catch(next);
  });
};
