{
  someTimeout: 2 * 60 * 1000,
  someDatabase: {
    database: 'someDatabase',
    user: 'user',
    password: 'password',
    host: '192.168.1.3',
    port: 5434
  },
  someQueue: {
    host: '192.168.1.4',
    port: 5000
  }
}
