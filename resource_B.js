const connectQueueSomehow = require('connectQueueSomehow');
const EventEmitter = require('events');

module.exports = class Resource_B {
  constructor (cfg) {
    this._someTimeout = cfg.someTimeout;
    this._connection = connectQueueSomehow(cfg.connectionConfig);
    this.whenReady = new Promise((resolve, reject) => {
      const timeout = setTimeout(() => reject(new Error('TIMEOUT')), this._someTimeout);
      this._connection.then(() => {
        clearTimeout(timeout);
        resolve();
      });
    });
    this._EventEmitter = new EventEmitter;

    this._connection.then(connection =>
      connection.onMessage(this._EventEmitter.emit.bind.(this._EventEmitter))
    );
  }

  send(something) {
    return this._connection
    .then(connection => connection.send(something));
  }

  on(message, handler) {
    this._EventEmitter.on(message, handler);
  }
};

