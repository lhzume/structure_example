const config = require('config');

const resourceA = new (require('./resourceA'))({
  connectionConfig: config.get('someDatabase'),
  someTimeout: config.get('someTimeout')
});
const resourceB = new (require('./resourceB'))({
  connectionConfig: config.get('someQueue'),
  someTimeout: config.get('someTimeout')
});

Promise.all([
  resourceA.whenReady,
  resourceB.whenReady
])
.then(() => {
  console.log('resources are ready');

  const serviceA = new (require('./service_A'))({
    resource: resourceA
  });
  const serviceB = new (require('./service_B'))({
    resource: resourceB
  });

  const controller = new (require('./controller'))({
    serviceA, serviceB
  });
})
.catch(error => {
  console.error(error);
  process.exit(1);
});
