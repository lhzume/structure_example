const crypto = require('crypto');

module.export = class ServiceA {
  constructor(cfg) {
    this._resource = cfg.resource;
  }

  doSomething(data) {
    const hash = crypto.createHash('sha256') 
      .update(data)
      .digest('hex');

    return this._resource.updateSomething(hash);
  }
};
