module.export = class ServiceB {
  constructor(cfg) {
    this._resource = cfg.resource;
    this._resource.on('message', this._handleMessage);
  }

  doSomething(data) {
    return this._resource.send(JSON.stringify(data));
  }

  _handleMessage(message) {
    // handles somehow
  }
};

