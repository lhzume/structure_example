const connectDatabaseSomehow = require('connectDatabaseSomehow');

module.exports = class Resource_A {
  constructor (cfg) {
    this._someTimeout = cfg.someTimeout;
    this._connection = connectDatabaseSomehow(cfg.connectionConfig);
    this.whenReady = new Promise((resolve, reject) => {
      const timeout = setTimeout(() => reject(new Error('TIMEOUT')), this._someTimeout);
      this._connection.then(() => {
        clearTimeout(timeout);
        resolve();
      });
    });
  }

  getSomething () {
    return this._connection
    .then(connection => connection.get('something'));
  }

  updateSomething () {
    return this._connection
    .then(connection => connection.update('something'));
  }
};
